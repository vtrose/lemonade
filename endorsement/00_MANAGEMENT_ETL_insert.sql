INSERT INTO bi_development.mng_etl_jobs
(job_name, session_id, user_name, status, etl_start_time)
SELECT 'fact_endorsement',
       current_session(),
       current_user(),
       'start',
       current_timestamp()
;