# import APIN
# import LocationsDB
import os
import sys
import requests
import pandas as pd
import datetime
from datetime import date
from datetime import timedelta
from datetime import datetime
import multiprocessing as mp
# pool = mp.Pool()
import json
from joblib import Parallel, delayed
from concurrent import futures
# import dask.dataframe as dd
# print("Number of processors: ", mp.cpu_count())
import pyodbc
import glob
from pathlib import Path
global_wd = os.path.realpath(__file__).replace(os.path.basename(os.path.realpath(__file__)),"")
# myDict = {}

# ######################################################################################
# ##function that recieves a location from the locations.csv table and exports data of ['date','location','min','max'] to csv tables 
# ##according to the number of rows in locations.csv table 
# ######################################################################################
def export_API_data_to_CSV(loc):
    myList = []
    print(loc)
    start_date = date(2013,1,1)#'2022-04-01'
    end_date   = date.today() - timedelta(days = 1)
    current_date = start_date
    while current_date <= end_date:
        year = current_date.year
        month = current_date.month
        day = current_date.day
        # print(str(year) + '/' +str(month)+ '/' +str(day))
        current_date = current_date + timedelta(days=1)

        command = 'https://www.metaweather.com/api/location/' + str(loc) + '/' + str(year) + '/' + str(month) + '/' + str(day) + '/'
        print(command)
        r = requests.get(command)
        # requests.get('https://www.metaweather.com/api/location/44418').json()
        # print(r.json())
        if len(r.json()) != 0:
            myDict = {}
            consolidated_weather = r.json()
            if  str(type(consolidated_weather)) == "<class 'list'>":
                consolidated_weather = consolidated_weather[0]
            # for key, value in consolidated_weather.items():       
            #     print('key: ', key,'value: ', value )
            myDict['date'] = current_date
            myDict['location'] = loc
            if consolidated_weather.get('min_temp') is not None:
                myDict['min'] = round(consolidated_weather.get('min_temp'),1)
            else: myDict['min'] = ''    
            if consolidated_weather.get('max_temp') is not None:
                myDict['max'] = round(consolidated_weather.get('max_temp'),1)
            else: myDict['max'] = '' 
            # sourceDF.at[i,'min_temp'] = round((consolidated_weather.get('min_temp')),1)
            # sourceDF.at[i,'max_temp'] = round((consolidated_weather.get('max_temp')),1)
            myList.append(myDict)
    myDF = pd.DataFrame(myList)
    myDF = myDF.set_index('date')
    print('finished with '+ str(loc) +'.csv file')
    #returns a dataframe of location with all it's dates
   
    # return myDF
    myDF.to_csv(global_wd + 'out_'+ str(loc) +'.csv')  
    




