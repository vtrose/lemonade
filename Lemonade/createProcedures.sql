IF OBJECT_ID('dbo.udf_alterTable') IS NOT NULL
DROP PROCEDURE [dbo].[udf_alterTable]
;

create procedure [dbo].[udf_alterTable]
--exec [dbo].[udf_alterTable] 'dbo.weather_data','[min]','float','''nan'''
--exec [dbo].[udf_alterTable] 'dbo.weather_data','[max]','float','''nan'''
(@tableName as varchar(100), @fieldName as varchar(50), @tgt as varchar(50), @cond as varchar(100))
as begin
declare @sqlUpdate varchar(max)= 'update ' + @tableName +
' set ' + @fieldName +
'='''' where '
+ @fieldName +'='+
@cond
--print @sqlUpdate 
exec (@sqlUpdate)

declare @sqlAlter varchar(max)= 'alter table  ' + @tableName +
' alter column ' + @fieldName + ' '+ @tgt
--print @sqlAlter 
exec (@sqlAlter)

end

;
IF OBJECT_ID('dbo.populate_country_agg_table') IS NOT NULL
DROP PROCEDURE dbo.populate_country_agg_table
;
create procedure [dbo].[populate_country_agg_table] 
--exec [dbo].[populate_country_agg_table] 
AS begin
if object_ID(N'dbo.country_agg') IS not NULL drop table dbo.country_agg

select [date], country, 
cast(MIN([min]) as decimal(5,2)) as min_temperature, 
cast(MAX([max]) as decimal(5,2)) as max_temperature, 
cast(AVG([min]) as decimal(5,2)) as avg_min_temperature, 
cast(AVG([max]) as decimal(5,2)) as avg_max_temperature
into [dbo].[country_agg]
from [dbo].[weather_data] w join [dbo].[Locations] l 
on w.[location]=l.[metaweather_id] 
group by [country], [date]
order by date
end