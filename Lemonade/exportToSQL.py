import os
import pandas as pd
# from joblib import Parallel, delayed
# from concurrent import futures
import pyodbc
from pathlib import Path
# from sqlite3 import OperationalError

# global_wd = os.path.realpath(__file__).replace(os.path.basename(os.path.realpath(__file__)),"")
global_wd = os.path.realpath(__file__).replace(os.path.basename(os.path.realpath(__file__)),"")
conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER=VITARO-LAP;DATABASE=Local;Trusted_Connection=yes;')

######################################################################################
##connect to MSSQL db, create the table if it doesn't exist
##receives a table name and it's columns as a list
######################################################################################
def DROPandCREATE(tablename='weather_data', data_top = ['date','location','min','max']):
    ######################################################################################
    # ######################################################################################
    curCreate = conn.cursor() 
    columns = ''
    for column in data_top:
        columns = columns +'[' + column + ']' + ' varchar (255),'
    # print(columns)
    columns = '('+columns[:-1]+')'    
    sqlQueryCreate = 'drop table if exists dbo.'+ tablename +';  CREATE TABLE dbo.'+ tablename + columns
    print(sqlQueryCreate)
    # print(sqlQueryUpdate)
    curCreate.execute(sqlQueryCreate) 
    conn.commit()
    curCreate.close()

    # curUpdate = conn.cursor()      
    # curUpdate.execute(sqlQueryUpdate)
    # conn.commit()
    # curUpdate.close()

##change type of max and min columns in weather_data table from varchar to float
def updateWeatherTable(tablename='weather_data'):
    curUpdate = conn.cursor() 
    sqlQueryUpdate = ' exec dbo.udf_alterTable \''+ tablename +'\', \'min\', \'float\', \'\'\'nan\'\'\' ;exec dbo.udf_alterTable \'dbo.weather_data\', \'max\', \'float\', \'\'\'nan\'\'\''
    print(sqlQueryUpdate)
    curUpdate.execute(sqlQueryUpdate) 
    conn.commit()
    curUpdate.close()

# ######################################################################################
# ##iterates over all the csv files and inserts their data into sql table that was previously created
# ######################################################################################    
def CSVtoSQL(folder=Path(global_wd).glob('out_'+"*.csv"), tablename = '[dbo].weather_data', data_top = ['date','location','min','max'], col_sep=','):
    cur = conn.cursor()    
    print(tablename)
    for filename in folder:
    # Create a table name
        df= pd.read_csv(filename,encoding = "utf-8", sep=col_sep)
        column_names = ",".join([str(i) for i in data_top])
        for i,row in df.iterrows():
            sqlQueryInsert = "INSERT INTO " + tablename + " (" +column_names + ") VALUES ('"+"','".join([str(i) for i in row])+"')"
            "','".join([str(i).replace("\t","''") for i in row])
            print(sqlQueryInsert)
            cur.execute(sqlQueryInsert)
        conn.commit()
    cur.close()
    # print(sqlQueryCreate)


# def alterTable(tablename='[dbo].weather_data'):
#     cur = conn.cursor()  
#     sqlQueryUpdate = 'exec alterTable_weather_data'

def executeScriptsFromFile(filename): 
    cur = conn.cursor()    
    fd = open(global_wd+'/'+filename, 'r')
    sqlFile = fd.read()
    fd.close()

    # all SQL commands (split on ';')
    sqlCommands = sqlFile.split(';')

    # Execute every command from the input file
    for command in sqlCommands: 
        cur.execute(command)
        conn.commit()
    cur.close()

def selectScriptsFromFile(filename): 
    cur = conn.cursor()    
    fd = open(global_wd+'/'+filename, 'r')
    sqlFile = fd.read()
    fd.close()
    # all SQL commands (split on ';')
    sqlCommands = sqlFile.split(';')
    # Execute every command from the input file
    for command in sqlCommands: 
        cur.execute(command)
        results = []
        columns = [column[0] for column in cur.description]
        for row in cur.fetchall():
            results.append(dict(zip(columns, row)))
        conn.commit()
    cur.close()
    return results    
    