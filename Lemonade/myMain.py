import os
from restAPI import export_API_data_to_CSV
from exportToSQL import CSVtoSQL, DROPandCREATE, updateWeatherTable, executeScriptsFromFile, selectScriptsFromFile
import pandas as pd
from pathlib import Path
import pyodbc
from joblib import Parallel, delayed

global_wd = os.path.realpath(__file__).replace(os.path.basename(os.path.realpath(__file__)),"")

file_path = global_wd + 'Locations.csv'
sourceDF= pd.read_csv(file_path, encoding = "utf-8", sep= '\t')
###connect to API and load data from there to csv files
##in a not parallel loop
# for i,l in sourceDF.iterrows(): 
#     export_API_data_to_CSV(sourceDF.iloc[i]['metaweather_id'])
##the same but parallel:
numberOfJobs = len(sourceDF)
Parallel(n_jobs=numberOfJobs)(delayed(export_API_data_to_CSV)(sourceDF.iloc[i]['metaweather_id']) for i,l in sourceDF.iterrows())

## create tables and procedures in sql server
## need to add a truncate or merge inser update 
# executeScriptsFromFile('createProcedures.sql')

DROPandCREATE()
DROPandCREATE(tablename='Locations', data_top = ['id','country','location','metaweather_id','created_at'])

#load the csv files previously imported into sql server db tables
folderName = Path(global_wd).glob('out_'+"*.csv")
CSVtoSQL(folderName)
CSVtoSQL(folder=Path(global_wd).glob('Locations.csv'), tablename = '[dbo].Locations', data_top = ['id','country','location','metaweather_id','created_at'], col_sep='\t')

#change type of max and min columns from varchar to float
updateWeatherTable(tablename='weather_data')


Q2 = selectScriptsFromFile('QuestionTwoQuery.sql')
print(Q2)
