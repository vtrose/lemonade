# import APIN
# import LocationsDB
import os
import sys
import requests
import pandas as pd
import datetime
from datetime import date
from datetime import timedelta
from datetime import datetime
import multiprocessing as mp
# pool = mp.Pool()
import json
from joblib import Parallel, delayed
from concurrent import futures
# import dask.dataframe as dd
# print("Number of processors: ", mp.cpu_count())
import pyodbc
import glob
from pathlib import Path
global_wd = os.path.realpath(__file__).replace(os.path.basename(os.path.realpath(__file__)),"")
# sys.path.append(global_wd)

file_path = global_wd + 'Locations.csv'

sourceDF= pd.read_csv(file_path, encoding = "utf-8", sep= '\t')
numberOfJobs = len(sourceDF)
myList = []
# myDict = {}

######################################################################################
##connect to MSSQL db, create the table if it doesn't exist   
######################################################################################
conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER=VITARO-LAP;DATABASE=Local;Trusted_Connection=yes;')
cur = conn.cursor()

tablename = 'Weather'
data_top = ['date','location','min','max']
# ######################################################################################
columns = ''
for column in data_top:
    columns = columns +'[' + column + ']' + ' varchar (255),'
print(columns)
columns = '('+columns[:-1]+')'    
sqlQueryCreate = 'drop table if exists dbo.'+ tablename +';  CREATE TABLE dbo.'+ tablename + columns
print(sqlQueryCreate)
cur.execute(sqlQueryCreate) 
conn.commit()
cur.close()
# with futures.ProcessPoolExecutor() as pool:

# ######################################################################################
# ##function that recieves a location from the locations.csv table and exports data of ['date','location','min','max'] to csv tables 
# ##according to the number of rows in locations.csv table 
# ######################################################################################
def export_API_data_to_CSV(loc):
    print(loc)
    start_date = date(2022,3,1)#'2022-04-01'
    end_date   = date.today() - timedelta(days = 1)
    current_date = start_date
    while current_date <= end_date:
        year = current_date.year
        month = current_date.month
        day = current_date.day
        # print(str(year) + '/' +str(month)+ '/' +str(day))
        current_date = current_date + timedelta(days=1)

        command = 'https://www.metaweather.com/api/location/' + str(loc) + '/' + str(year) + '/' + str(month) + '/' + str(day) + '/'
        print(command)
        
        r = requests.get(command)
        # print(r.json())
        if len(r.json()) != 0:
            myDict = {}
            consolidated_weather = r.json()
            if  str(type(consolidated_weather)) == "<class 'list'>":
                consolidated_weather = consolidated_weather[0]
            # for key, value in consolidated_weather.items():       
            #     print('key: ', key,'value: ', value )
            myDict['date'] = current_date
            myDict['location'] = loc
            if consolidated_weather.get('min_temp') is not None:
                myDict['min'] = round(consolidated_weather.get('min_temp'),1)
            else: myDict['min'] = ''    
            if consolidated_weather.get('max_temp') is not None:
                myDict['max'] = round(consolidated_weather.get('max_temp'),1)
            else: myDict['max'] = '' 
            # sourceDF.at[i,'min_temp'] = round((consolidated_weather.get('min_temp')),1)
            # sourceDF.at[i,'max_temp'] = round((consolidated_weather.get('max_temp')),1)
            myList.append(myDict)
    myDF = pd.DataFrame(myList)
    myDF = myDF.set_index('date')
    print('finished with '+ str(loc) +'.csv file')
    #returns a dataframe of location with all it's dates
   
    # return myDF
    myDF.to_csv(global_wd + 'out_'+ str(loc) +'.csv')  
    

######################################################################################

# for i,l in sourceDF.iterrows(): 
#     # x = sourceDF.iloc[i]['metaweather_id']
#     # # print(type(x))
#     # export_API_data_to_CSV(x)
#     export_API_data_to_CSV(sourceDF.iloc[i]['metaweather_id'])
# ##the same but parallel:
# ######################################################################################
# # Parallel(n_jobs=numberOfJobs)(delayed(export_API_data_to_CSV)(sourceDF.iloc[i]['metaweather_id']) for i,l in sourceDF.iterrows())

# ######################################################################################
# ##iterates over all the csv files and inserts their data into sql table that was previously created
# ######################################################################################    
tablename = '[dbo].'+tablename
print(tablename)
for filename in Path(global_wd).glob('out_'+"*.csv"):
# Create a table name
    df= pd.read_csv(filename,encoding = "utf-8")
    column_names = ",".join([str(i) for i in data_top])
    for i,row in df.iterrows():
        sqlQueryInsert = "INSERT INTO " + tablename + " (" +column_names + ") VALUES ('"+"','".join([str(i) for i in row])+"')"
        print(sqlQueryInsert)
        cur.execute(sqlQueryInsert)
    conn.commit()
cur.close()
    # print(sqlQueryCreate)



