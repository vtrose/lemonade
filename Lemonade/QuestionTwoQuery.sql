select a.*, 
country, c.location, b.max as temperature,
cast(average_MAX_Total-b.max as decimal(5,2)) as [difference]
from(
SELECT [date],
cast(avg([avg_max_temperature]) as decimal(5,2)) as average_MAX_Total
FROM [Local].[dbo].[country_agg]
group by date
--order by date
) a 
join [dbo].[weather_data] b on a.date=b.date
join [dbo].[Locations] as c on c.metaweather_id=b.location
order by date